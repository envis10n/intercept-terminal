// Setup globals

// Add ability to easily generate random integers.
Math.randomInRange = function(max = 1, min = 0){
    return Math.round(Math.random()*(max-min)+min);
}