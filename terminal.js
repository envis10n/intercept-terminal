var term = require('terminal-kit').terminal;
const strip_ansi = require('strip-ansi');
module.exports.input = null;
var p_default = 'root@localhost ';
var p = 'root@localhost ';

term.on('key', (name)=>{
    if(name == 'CTRL_C'){
        term.processExit();
    }
});

function rebaseInput(){
    if(module.exports.input != null){
        term.eraseLine();
        module.exports.input.hide();
        term.moveTo(1, term.height);
        term(colors(p));
        module.exports.input.rebase(p.length+1, term.height);
        module.exports.input.show();
    }
}

module.exports.set_prompt = function(prompt){
    p_default = prompt;
    p = prompt;
    rebaseInput();
}

term.on('resize', (width, height)=>{
    rebaseInput();
});

const colors = require('./color');

module.exports.clear = function(){
    term.clear();
}

module.exports.menu = function(items, ...cbs){
    term.singleColumnMenu(items, (err, res)=>{
        if(err) throw err;
        cbs[res.selectedIndex]();
    });
}

function writeText(text){
    return new Promise((resolve, reject)=>{
        console.log(colors(text));
        resolve();
    });
}

module.exports.print = function(text){
    return new Promise((resolve, reject)=>{
        if(!text || text == '') {
            resolve();
        } else {
            if(module.exports.input != null){
                term.eraseLine();
                module.exports.input.hide();
            }
            term('\r');
            writeText(text).then(()=>{
                if(module.exports.input != null){
                    term(colors(strip_ansi(p)));
                    module.exports.input.rebase(p.length+1, term.height);
                    module.exports.input.show();
                }
                resolve();
            });
        }
    });
}

module.exports.inputPrompt = function(prompt, opts, rebase = false){
    return new Promise((resolve, reject)=>{
        p = prompt;
        term(colors(p));
        module.exports.input = term.inputField(opts, (err, res)=>{
            if(err) throw err;
            term('\n');
            module.exports.input = null;
            p = p_default;
            resolve(res);
        });
        module.exports.input.on('ready', ()=>{
            if(rebase){
                rebaseInput();
                module.exports.input.emit('ready-init');
            }
        });
    });
}