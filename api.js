const EventEmitter = require('events');

module.exports = function(intercept){
    var API = {};
    intercept.locked = false;
    API.setMacro = function(name, val){
        intercept.macros[name] = val;
    }

    API.sendChatMessage = function(channel, msg){
        var cmd = `chats send ${channel} ${msg}`;
        intercept.SendCommandAuto(cmd);
    }

    API.hooks = new EventEmitter();

    API.registerCommand = function(name, cb){
        intercept.client_commands.set(name, cb);
    }

    API.sendCommand = function(cmd){
        intercept.SendCommandAuto(cmd);
    }

    API.print = function(text){
        intercept.print(text);
    }

    API.lock = function(val = false){
        intercept.locked = Boolean(val);
    }

    Object.freeze(API);

    // API hooks
    intercept.on('json', (dobj)=>{
        switch(dobj.event){
            case 'info':
                API.hooks.emit('info', dobj);
            break;
            case 'auth':
                API.hooks.emit('auth', dobj);
            break;
            case 'traceStart':
                API.hooks.emit('traceStart', dobj);
            break;
            case 'traceComplete':
                API.hooks.emit('traceComplete', dobj);
            break;
            case 'cfg':
                API.hooks.emit('cfg', dobj);
            break;
            case 'connect':
                API.hooks.emit('connect', dobj);
            break;
            case 'broadcast':
                API.hooks.emit('broadcast', dobj);
            break;
            case 'chat':
                var msg = dobj.msg;
                var start = msg.indexOf('(', 0);
                var end = msg.indexOf(')', start);
                var channel = msg.substring(start+1, end);
                API.hooks.emit('chat', {event: 'chat', msg: dobj.msg, channel: channel});
            break;
            case 'error':
                API.hooks.emit('error', dobj);
            break;
            case 'connected':
                API.hooks.emit('connected', dobj);
            break;
            case 'command':
                API.hooks.emit('command', dobj);
                API.hooks.emit(dobj.cmd, dobj.msg);
            break;
        }
    });

    return API;
}