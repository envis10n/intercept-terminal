require('./globals'); // Load globals.

var terminal = require('./terminal');

const fs = require('fs');

fs.writeFileSync('./net.log', '');

const Intercept = require('intercept-lib');

terminal.clear();

var intercept = new Intercept();

intercept.macros = {};

intercept.client_commands = new Map();

intercept.print = function(text){
    terminal.print(text);
}

const API = require('./api')(intercept);

const root = `${process.cwd()}/plugins`;

if(!fs.existsSync(root)){
    fs.mkdirSync(root);
}

function getPlugins(){
    var dir = fs.readdirSync(root);
    var t = [];
    dir.forEach(fname=>{
        var file = `${root}/${fname}`;
        var stat = fs.statSync(file);
        if(stat.isDirectory()){
            t.push(file);
        }
    });
    return t;
}

function loadPlugin(dir){
    try {
        var meta = JSON.parse(fs.readFileSync(`${dir}/plugin.json`));
        require(`${dir}/plugin.js`)(API);
        terminal.print(`Loaded plugin: ${meta.name}`);
    } catch(e){
        console.log(e);
    }
}

getPlugins().forEach(plugin=>{
    loadPlugin(plugin);
});

var autocompletes = [
    'cmds',
    'help',
    'chats',
    'chats send',
    'chats list',
    'chats join',
    'chats leave',
    'chats create',
    'chats admin',
    'chats info',
    'probe',
    'breach',
    'web',
    'antivirus',
    'getpw',
    'pass see',
    'pass reset',
    'trace',
    'software',
    'software list',
    'software install',
    'software uninstall',
    'specs',
    'connect',
    'trace',
    'malware',
    'ports',
    'scan',
    'hardware',
    'hardware upgrade_cpu',
    'hardware upgrade_ram',
    'hardware upgrade_ports',
    'bits',
    'bits balance',
    'bits transfer',
    'jobs',
    'jobs list',
    'jobs kill',
    'mkdir',
    'rm',
    'rmdir',
    'ls',
    'cat',
    'abandon',
    'clear',
    'vol',
    'exit',
    'dc',
    'disconnect',
    'slaves',
    'slaves list',
    'malware',
    'honeypot',
    'connect 0.0.0.0 0p3nD00r'
];

if(fs.existsSync('./autocompletes.txt')){
    autocompletes = fs.readFileSync('./autocompletes.txt').toString().split('\n');
}

var history = [];

if(fs.existsSync('./history.txt')){
    history = fs.readFileSync('./history.txt').toString().split('\n');
}

var ip = '';
var cip = '';
var prompt = 'root@localhost ';

intercept.on('json', (dobj)=>{
    fs.appendFile('./net.log', JSON.stringify(dobj)+'\n', ()=>{});
    switch(dobj.event){
        case 'broadcast':
            if(dobj.msg == ''){
                terminal.print('Bitminer generated bits.');
            } else {
                terminal.print(dobj.msg);
            }
        break;
        case 'chat':
            terminal.print(dobj.msg);
        break;
        case 'connect':
            if(dobj.player) {
                ip = dobj.player.ip;
                cip = dobj.player.conn;
                prompt = `root@${ip == cip ? 'localhost' : cip} `;
                terminal.set_prompt(prompt);
            }
        break;
        case 'connected':
            ip = dobj.player.ip;
            cip = dobj.player.conn;
            prompt = `root@${ip == cip ? 'localhost' : cip} `;
            terminal.set_prompt(prompt);
        break;
    }
});

function addAutocomplete(text){
    if(!autocompletes.find(el=>{
        return el == text;
    })){
        autocompletes.push(text);
        fs.writeFile('./autocompletes.txt', autocompletes.join('\n'), err=>{
            if(err) throw err;
        });
    }
}

function addHistory(text){
    if(history[history.length-1] != text){
        history.push(text);
        fs.writeFile('./history.txt', history.join('\n'), err=>{
            if(err) throw err;
        });
    }
}

function macroReplace(txt){
    Object.keys(intercept.macros).forEach((name)=>{
        var reg = new RegExp(`[$]${name}`, 'g');
        txt = txt.replace(reg, intercept.macros[name]);
    });
    return txt;
}

async function loop(init){
    terminal.inputPrompt(prompt, {minLength: 1, history: history, autoComplete: autocompletes, autoCompleteHint: true}, true).then((command)=>{
        command = macroReplace(command);
        addHistory(command);
        if(command == 'clear') {
            terminal.clear();
            loop();
        } else {
            if(command[0] == ':'){
                command = command.substring(1);
                try {
                    var res = eval(`${command}`);
                    if(res != undefined){
                        switch(typeof res){
                            case 'number':
                            case 'boolean':
                                res = res.toString();
                            break;
                            case 'object':
                                res = JSON.stringify(res, null, '\t');
                            break;
                            default:
                                res = res.toString();
                            break;
                        }
                        terminal.print(res).then(()=>{
                            loop();
                        });
                    } else {
                        loop();
                    }
                } catch(e){
                    terminal.print(e.message).then(()=>{
                        loop();
                    });
                }
            } else if(command[0] == '/'){
                command = command.substring(1);
                var cmd = command.split(' ')[0];
                var args = command.split(' ').slice(1);
                var c = intercept.client_commands.get(cmd);
                if(c){
                    var res = c(args);
                    if(res != undefined) {
                        if(res instanceof Promise){
                            res.then((re)=>{
                                if(typeof re == 'string' || typeof re == 'number'){
                                    
                                } else if(typeof re == 'object') {
                                    re = JSON.stringify(re, null, '\t');
                                } else {
                                    re = re.toString();
                                }
                                terminal.print(re).then(()=>{
                                    loop();
                                });
                            });
                        } else {
                            if(typeof res == 'string' || typeof res == 'number'){
                                
                            } else if(typeof res == 'object') {
                                res = JSON.stringify(res, null, '\t');
                            } else {
                                res = res.toString();
                            }
                            terminal.print(res).then(()=>{
                                loop();
                            });
                        }
                    } else {
                        loop();
                    }
                } else {
                    terminal.print(`¬RInvalid client command`).then(()=>{
                        loop();
                    });
                }
            } else if(command[0] == '!'){
                if(command.length == 1){
                    terminal.print(`[--Macro List--]\n${Object.keys(intercept.macros).map(el=>{
                        return `¬g$${el} ¬v=> ¬b${intercept.macros[el]}`;
                    }).join('\n')}`);
                    loop();
                } else {
                    command = command.substring(1);
                    var name = command.split(' ')[0];
                    var val = command.split(' ').slice(1).join(' ');
                    if(val.length > 0){
                        intercept.macros[name] = val;
                        terminal.print(`Macro ${name} set to: ${val}`);
                        loop();
                    } else {
                        terminal.print(intercept.macros[name] != undefined ? intercept.macros[name] : 'Macro not set');
                        loop();
                    }
                }
            } else {
                if(intercept.locked){
                    terminal.print('Command lock is active.');
                    loop();
                } else {
                    var cmd = command.split(' ')[0];
                    function printError(dobj){
                        terminal.print(dobj.error);
                        intercept.removeAllListeners(`return_${cmd}`);
                        loop();
                    }
                    intercept.once(`return_${cmd}`, data=>{
                        terminal.print(data.replace(/\u200b/g, ' ').replace(/\t/g, ' '));
                        API.hooks.removeListener('error', printError);
                        loop();
                    });
                    API.hooks.once('error', printError);
                    intercept.SendCommand(command);
                }
            }
        }
    });
    if(init){
        terminal.input.on('ready-init', ()=>{
            terminal.print(init);
            terminal.set_prompt(prompt);
        });
    }
}

intercept.on('login', (dobj)=>{
    terminal.clear();
    loop(dobj.msg);
});

intercept.on('badLogin', err=>{
    terminal.clear();
    intercept.emit('ready', err);
});

intercept.on('ready', (msg)=>{
    terminal.print(msg);
    terminal.menu(['Login', 'Register'], async ()=>{
        var username = await terminal.inputPrompt('login: ', {});
        var password = await terminal.inputPrompt('password: ', {echoChar: true});
        intercept.Login({
            username: username,
            password: password
        });
    }, async ()=>{
        var username = await terminal.inputPrompt('username: ', {});
        var password = await terminal.inputPrompt('password: ', {echoChar: true});
        intercept.Register({
            username: username,
            password: password
        });
    });
});